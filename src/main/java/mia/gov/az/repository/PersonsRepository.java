package mia.gov.az.repository;


import mia.gov.az.domain.Persons;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PersonsRepository  extends JpaRepository<Persons,Long> {



        @Override
        Page<Persons> findAll(Pageable pageable);


        @Query("select id from Persons where id= :id")
        Long id(@Param("id")Long id);


        @Query("select p from Persons p where p.name  like %:search% or p.surname like %:search%")
     Page<Persons>findPersonsByNameOrSurnameContainingIgnoreCase(Pageable pageable,@Param("search")String search);
}

