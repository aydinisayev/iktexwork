package mia.gov.az.domain;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "persons")
public class Persons implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "birthdate")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date birthdate;
    @Size(max = 255)
    @Column(name = "gender")
    private String gender;
    @Size(max = 255)
    @Column(name = "name")
    private String name;
    @Size(max = 255)
    @Column(name = "patronymic")
    private String patronymic;
    @Size(max = 255)
    @Column(name = "place_of_birth")
    private String placeOfBirth;
    @Size(max = 255)
    @Column(name = "place_of_residence")
    private String placeOfResidence;
    @Size(max = 255)
    @Column(name = "surname")
    private String surname;
    @JoinColumn(name = "partiya_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Partiya partiyaId;
    @JoinColumn(name = "workplace_id", referencedColumnName = "personid")
    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private WorkPlace workplaceId;

    public Persons() {
    }

    public Persons(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getPlaceOfResidence() {
        return placeOfResidence;
    }

    public void setPlaceOfResidence(String placeOfResidence) {
        this.placeOfResidence = placeOfResidence;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Partiya getPartiyaId() {
        if(partiyaId == null){
             partiyaId = new Partiya();
        }
        return partiyaId;
    }

    public void setPartiyaId(Partiya partiyaId) {
        this.partiyaId = partiyaId;
    }

    public WorkPlace getWorkplaceId() {
        if(workplaceId == null){
             workplaceId = new WorkPlace();
        }
        return workplaceId;
    }

    public void setWorkplaceId(WorkPlace workplaceId) {
        this.workplaceId = workplaceId;
    }
//
//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (id != null ? id.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof Persons)) {
//            return false;
//        }
//        Persons other = (Persons) object;
//        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "models.Persons1[ id=" + id + " ]";
//    }

    @Override
    public String toString() {
        return "Persons{" +
                "id=" + id +
                ", birthdate=" + birthdate +
                ", gender='" + gender + '\'' +
                ", name='" + name + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", placeOfBirth='" + placeOfBirth + '\'' +
                ", placeOfResidence='" + placeOfResidence + '\'' +
                ", surname='" + surname + '\'' +
                ", partiyaId=" + partiyaId +
                ", workplaceId=" + workplaceId +
                '}';
    }
}
