package mia.gov.az.controller;


import mia.gov.az.domain.User;
import mia.gov.az.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class RegistrationController {





    @Autowired
    UserService userService;




    @GetMapping("/registration")
    public String registration(Model model){

        model.addAttribute("user",new User());
        return "registration";
    }



    @PostMapping("/registration")
  public String addUser(
                        @Valid User user,
                        BindingResult bindingResult,
                        Model model)
    {


        if(user.getPassword()!=null&&!user.getPassword().equals(user.getPassword2())){
            model.addAttribute("password2Error","Passwords are different!");

        }
        if(user.getPassword()!=null&&!user.getPassword().equals(user.getPassword2())|| bindingResult.hasErrors()){


            return "registration";
        }


        if(!userService.addUser(user)){
    model.addAttribute("usernameError","user exists");
    return "registration";
}

        return "redirect:login";
    }


}
