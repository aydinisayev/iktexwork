package mia.gov.az.controller;


import mia.gov.az.domain.Persons;
import mia.gov.az.repository.PersonsRepository;
import mia.gov.az.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class MainController {




    @Autowired
PersonsRepository personsRepository;


    @Autowired
    UserRepository userRepository;






    @GetMapping("/")
    public String input(Model model, @RequestParam(name="search",required = false)String search,@PageableDefault(sort={"id"},direction= Sort.Direction.DESC) Pageable pageable){





        model.addAttribute("person",new Persons());
        Page<Persons> all =search==null?personsRepository.findAll(pageable):personsRepository.findPersonsByNameOrSurnameContainingIgnoreCase(pageable,search.trim());
        model.addAttribute("page",all);



        return "main";
    }


    @PostMapping("/")
    public String saveupdate(@ModelAttribute(name = "person")  Persons persons){

        personsRepository.save(persons);
        return "redirect:/";
    };



   @GetMapping("/edit")
    public String edit(Model model,@RequestParam(name = "all")Persons persons,@PageableDefault(sort={"id"},direction= Sort.Direction.DESC) Pageable pageable){
     model.addAttribute("edit","edit");
     model.addAttribute("page",personsRepository.findAll(pageable));
     model.addAttribute("person",persons);

       return "main";

    }
@PostMapping("/delete")
    public String delete (@RequestParam(name="all")Long id){

      if(personsRepository.id(id)!=null){
          personsRepository.deleteById(id);
          return "redirect:/";
      }

       return "redirect:/";


}

   }





